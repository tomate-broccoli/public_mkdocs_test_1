# 注釈

ほげほげ[^1]

[^1]:ほげほげとは本来。。。。

## mermaid.js

h1だと左ペインにも右ペインにも表示されない。
h2だと右ペインに表示される。

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```
