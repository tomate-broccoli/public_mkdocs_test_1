# はじめに

- どうもdocs/直下にindex.mdがないと表示がおかしくなるっぽい。
- メニューに表示される名称は、デフォルトだと以下になるもよう。
    - index.mdの最初のh1の文言
    - フォルダの名称
        - 先頭文字が大文字になる
        - アンダスコアはスペースに置換される
- タイトルにもindex.mdへのリンクが作成されるので、メニューからindex.mdの項目は削除してほしい。
    - サブフォルダが無い場合はメニューにindex.mdの項目は表示されない？？？？
    - これは、←Previous,Next→があるためしょうが無いのか？


## リンク[^1]

- [mkdocs](https://github.com/mkdocs/mkdocs/){target=_blank} 
- [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html){:target=_blank} 
- [Yahoo Japan](https://www.yahoo.co.jp){:target="_blank"} 

[^1]:mkdocsはちゃんと外部リンク({target=_blank})の記述は表示から消えていい感じなんだけど、  
gitlabは外部リンクの記述が表示されたままでいけてない。。。

### ３段目

- h3はサイドペインには表示されまいっぽい。
- mkdocsは注釈がうまく機能しなかった。。。
- mkdocsは4スペースないと段落を認識しないもよう。。。
